# README #

### Project info

Eureka server that keeps track of up and running instances of applications. (https://github.com/Netflix/eureka, https://github.com/spring-cloud/spring-cloud-netflix)

Uses:
- Java 11
- Gradle 6.6.1 (use the gradlew wrapper)
- Spring Boot 2.3.4
- Spring Cloud Hoxton.SR8
- Lombok (https://projectlombok.org/)

### Developing

Commandline:
- gradlew build -> Builds and tests the application
- gradlew bootBuildImage -> Generates a Docker image
- gradlew bootRun -> Runs the application